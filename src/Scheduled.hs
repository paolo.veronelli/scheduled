
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NoImplicitPrelude, TemplateHaskell #-}

module Scheduled where

import Control.Concurrent
import Control.Lens
import Control.Concurrent.Async
import Control.Concurrent.STM
import Protolude hiding ( FilePath )
import Pipes.Safe
import Data.Time.Clock.System
import qualified Data.Map.Strict as M

data Job = Job 
    {   job_offset :: Int64
    ,   job_every :: Int64 
    ,   job_job :: IO ()
    ,   _job_running :: Bool
    ,   _job_run :: Maybe Int64
    ,   job_identifier :: Text
    }

newtype  Scheduler = Scheduler 
    {   jobs :: Map Int Job
    }

makeWrapped ''Scheduler 

makeLenses ''Job 

fork :: IO a -> IO ()
fork f = async f >>= link 

fire :: Eq a => a -> Maybe a -> Bool
fire _ Nothing = True
fire secs (Just secs') = secs /= secs' 

runScheduler ::  (Text -> SomeException -> IO ()) -> IO (TVar Scheduler)
runScheduler report = do 
    scheduler <- newTVarIO $ Scheduler mempty
    fork $ forever $ do 
        MkSystemTime secs _ <-  getSystemTime
        Scheduler js <- readTVarIO scheduler 
        iforM_ js $ \index' -> \case 
            Job offset (mod secs . (+) offset -> 0) action False (fire secs -> True) id
                -> do 
                    atomically $ modifyTVar scheduler $ _Wrapped . ix index' %~ 
                         (job_run ?~  secs) . (job_running .~ True)

                    fork do 
                        catchAll action (report id)
                        atomically $ modifyTVar scheduler $ _Wrapped . ix index' %~ 
                          (job_running .~ False)
            _ -> pure ()
        threadDelay 100_000 
    pure scheduler

schedule :: TVar Scheduler -> Text -> Int64 -> Int64 -> IO () -> IO ()
schedule scheduler job_identifier job_offset job_every job_job 
    = atomically $ modifyTVar scheduler $ over _Wrapped $ \m -> 
        let greatest = maybe 0 fst $ M.lookupMax m 
            _job_running = False
            _job_run = Nothing
        in M.insert (succ greatest) Job {..} m

